import assert from "node:assert";
import fs from "node:fs";
import { spawn } from "node:child_process";

const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const sassCmd = spawn("npm", ["run", "sass"], {stdio: 'inherit'});
await sleep(500);

fs.writeFileSync("./other-src/_partial.scss", "body { color: red }");
await sleep(500);
const result = fs.readFileSync("./dist/index.css").toString();

try {
  assert.equal(
    result,
    `body {\n  color: red;\n}\n\n/*# sourceMappingURL=index.css.map */\n`
  );
} finally {
  sassCmd.kill();
  fs.writeFileSync("./other-src/_partial.scss", "body { color: yellow }");
}
